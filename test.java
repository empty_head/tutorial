package test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import org.apache.bsf.BSFException;
import org.jruby.Ruby;
import org.json.JSONException;
import org.json.JSONObject;


public class Test {

    private final static String FILES_DIR = "C:\\for_test";

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws JSONException, IOException, BSFException, InterruptedException {

        Test t = new Test();
        t.operation_part1();
        //t.operation_part2();

    }

    public void operation_part1() throws BSFException, IOException, InterruptedException {

        String f = "C:\\Users\\anton_starovoytov\\Documents\\NetBeansProjects\\Test\\tabula\\table_extractor.rb";
        
        /*
        PDDocument d = PDDocument.load(new File(p));
        List l = d.getDocumentCatalog().getAllPages();
        File outputfile = new File("image.jpg");
        BufferedImage bi = ((PDPage)l.get(0)).convertToImage();
        ImageIO.write(bi, "jpg", outputfile);
        */
        
        //String p = "./book.pdf";
        String p = "C:\\acba group_2011english-1438176307-.pdf";

        final Ruby runtime = Ruby.getDefaultInstance();
        runtime.defineModule(f);
        
        //tabula --no-spreadsheet --pages all --format JSON --outfile <outfilename> <pdffilename>

        runtime.addToObjectSpace(true, runtime.evalScriptlet(getFileContents("tabula.rb")));
        runtime.addToObjectSpace(true, runtime.evalScriptlet(getFileContents("./tabula/writers.rb")));
        
        String parse_string = "Tabula.extract_table('" + p + "', :all, [0, 0, 100000, 1000000], {\"extraction_method\"=>'no-spreadsheet'})";
        
        //runtime.evalScriptlet("Tabula::Writers::JSON( " + parse_string + ", File.open('out.txt', 'a') )");
        runtime.evalScriptlet("Tabula::Writers::JSON( " + parse_string + " )");
        
    }

    private String getFileContents(String filename) throws IOException {
        FileInputStream is = null;
        try {
            File f = new File(filename);
            is = new FileInputStream(f);
            InputStreamReader isr = new InputStreamReader(is);
            BufferedReader br = new BufferedReader(isr);
            StringBuilder cnt = new StringBuilder();
            String t;
            while ((t = br.readLine()) != null) {
                cnt.append(t);
                cnt.append("\n");
            }

            return cnt.toString();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return "";
    }

    public void operation_part2() throws IOException, JSONException {

        File[] files = (new File(FILES_DIR)).listFiles();

        for (File file : files) {

            String content = getFileContent(file.getPath());
            System.out.println(getTable(content));

        }

    }

    private String getValue(String s) {

        s = s.substring(s.indexOf("\"text\":") + 8);
        s = s.substring(0, s.length() - 1);

        return s.trim();
    }

    private JSONObject getTable(String s) throws JSONException {
        JSONObject new_table = new JSONObject();

        List<String> objects = getObjects(s);

        for (String object : objects) {
            new_table.append("tables", getRows(object));
        }

        return new_table;
    }

    private JSONObject getRows(String s) throws JSONException {
        JSONObject new_table = new JSONObject();

        List<String> objects = getObjects(s);

        for (String object : objects) {
            new_table.append("rows", getColumns(object));
        }

        return new_table;
    }

    private JSONObject getColumns(String s) throws JSONException {
        JSONObject new_row = new JSONObject();
        String[] values = s.substring(1, s.length() - 1).split("\\}\\,\\{");

        for (String value : values) {
            JSONObject new_column = new JSONObject();
            new_column.put("val", getValue(value));
            new_column.put("header", "");

            new_row.append("columns", new_column);
        }

        return new_row;
    }

    private List<String> getObjects(String s) throws JSONException {

        s = s.trim();

        int end_object = -1;
        int counter = 0;

        List<String> objects = new ArrayList();

        while (s.length() > 0) {
            for (int i = 0; i < s.length(); i++) {
                if (s.charAt(i) == '[') {
                    counter++;
                }
                if (s.charAt(i) == ']') {
                    counter--;
                }
                if (counter == 0) {
                    end_object = i + 1;

                    if (i > 0) {
                        objects.add(s.substring(1, end_object - 1));
                    }

                    s = s.substring(end_object);
                    i = 0;
                    break;
                }
            }
        }

        return objects;
    }

    private String getFileContent(String file_name) throws FileNotFoundException, IOException {
        FileInputStream is = null;

        File f = new File(file_name);
        is = new FileInputStream(f);
        InputStreamReader isr = new InputStreamReader(is);
        BufferedReader br = new BufferedReader(isr);
        StringBuilder cnt = new StringBuilder();

        String t;
        while ((t = br.readLine()) != null) {
            cnt.append(t);
            cnt.append("\n");
        }

        return cnt.toString();

    }

}
